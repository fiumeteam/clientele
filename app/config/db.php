<?php
/* DB Config */
/* change dbname to your database name */
ORM::configure('mysql:host=localhost;dbname=clientele');
/* enter username to access the database */
ORM::configure('username', 'root');
/* enter password to access the database */
ORM::configure('password', '');
/* this is the ORM feature to return results sets [leave this true] */
ORM::configure('return_result_sets', true);
/* this is the ORM feature to set database to UTF8 */
ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
