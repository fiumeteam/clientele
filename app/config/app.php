<?php

/**
 * Constants
 */
// 1.1 Enter Full Site URL HERE
define("site_url", "http://localhost/clientele/");

// 1.2 Photo Uploading Path Here
// Example
// define("upload_path", "/home3/fiumein/public_html/projects/hype/uploads/promotions/");
define("upload_path", "C:\\wamp\www\\clientele\\uploads\\files\\");

/**
 * Session Start
 */
session_start();
// Registering Slim Autoload
\Slim\Slim::registerAutoloader();

$app = new \SlimController\Slim(array('templates.path'   => dirname(__DIR__).'/views/',
                            'cookies.secret_key'         => md5('appsecretkey'), 
                            'controller.class_prefix'    => '',
                            'controller.method_suffix'   => '',
                            'controller.template_suffix' => 'html',
                            'mode' => 'production',
                            'debug' => true
                     ));


if ($app->config('debug') === true) {
    ini_set('display_errors', 'On');
    error_reporting(E_ALL ^ E_NOTICE); 
}
/**
 * Require all models
 */
foreach (glob(dirname(__DIR__)."/models/*.php") as $filename) {
    require $filename;
}

/**
 * Require all controllers
 */
foreach (glob(dirname(__DIR__)."/controllers/*.php") as $filename) {    
    require $filename;
}
/**
 * Require hooks
 */
require dirname(__DIR__)."/hooks.php";
/**
 * Require routes
 */
require dirname(__DIR__)."/routes.php";

$app->run(); 
