<?php
/* Routes */
// Index Route
$app->addRoutes(array(
	'/' => 'App:index',
));

/* group routes 
example: [api is the group]
** /api/photos 
** /api/users
** /api/login
*/


$app->group('/api', 'APIrequest', function () use ($app) {

	$app->addRoutes(array(
	'/test' => 'AppApi:index',
	'/:project/logos' => 'AppApi:projectLogos',
	'/:project/logos/:id' => 'AppApi:smallLogos',
));

// Single routes (GET)
// 		$app->addRoutes(array(
// 			'/users' => 'AppApi:users',
// 		));

// advanced Routes (POST)
// 		$app->addRoutes([
// 			'/promotions/new' => [
// 		    'post' => ['AppApi:newPromo']
// 			],
// 		]);

	});
