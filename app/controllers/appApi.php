<?php 
use Gregwar\Image\Image;
class AppApi extends \SlimController\SlimController
{


	 public function index()
    {
    	$this->render(200, array(
    		"message" => "API endpoint"
    	));
    }

     public function projectLogos($title)
    {

    	$projects = Model::factory('Projects')->where('title', $title)->find_one();
    	$logos = $projects->logos()->select(['id', 'slno'])->find_many();

    	$arr = [];

    	foreach ($logos as $logo) {
    		$arr[] = [
    			"id" => $logo->id,
    			"slno" => $logo->slno,
    		];
    	}


    	$this->render(200, array(
    		"logos" => $arr
    	));
    }
 public function smallLogos($project, $id)
    {

        $logo = Model::factory('Logos')->where('id', $id)->find_one();
        // $Previews = Model::factory('Previews')->where('logos_id', $id)->find_many();
        // $previews = $logos->previews()->select(['id'])->find_many();
          if($logo)
          {


        $arr = [
            "id" => $logo->id,
            "files" => [
                "thumb" => Image::open(upload_path.$logo->File())->resize(600)->guess(),
                "large" => Image::open(upload_path.$logo->File())->guess()
            ]
        ];

        foreach ($logo->GetPreviews() as $preview) {
            $arr['previews'][] = [
                "id" => $preview->id,
                "title" => $preview->title,
                "file" => Image::open(upload_path.$preview->file())->guess(),
            ];
        }

        // foreach ($logos as $logo) {
        //     $arr[] = [
        //         "id" => $logo->id,
               
        //     ];
        // }


        $this->render(200, array(
            "logos" => $arr
        ));
        }
        else
        {
           $this->render(200, array(
            "message" => "File Not Found"
        )); 
        }
    }


	}
