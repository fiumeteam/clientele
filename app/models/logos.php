<?php
// http://paris.readthedocs.org/en/latest/models.html
class Logos extends Model {
	
	public function previews()
	{
		return $this->has_many('Previews');
	}


	public function File()
	{
		$file = File::where('id', $this->file_id)->find_one();
		return $file->url;
	}
     public function GetPreviews()
	{
		$previews = Previews::where('logos_id', $this->id)->find_many();
		return $previews;
	}


}