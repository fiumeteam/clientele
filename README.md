SLIM SKELETON
=============

Get Started
-------------

** Step 1 **
run **composer install**

** Step 2 **
go to app > config > app.php
change line 7 and 12 as per your requirement


** Step 3 **
go to app > config > db.php and enter your database connections informations.


App Api
------------

** 1. api/projects/:clientele/logos **

response

{
	logos: array("id", "slno"),
	error: false,
	status: 200
}


** 1. api/projects/:clientele/logos/:id **

response

{
	logos: array("id", array("files" => array("thumb", "large")), array("previews" => array("id", "title", "file")), ),
	error: false,
	status: 200
}




**SLIM FRAMEWORK :**  http://docs.slimframework.com/

**SLIM CONTROLLER:**  https://github.com/fortrabbit/slimcontroller

    $app->addRoutes(array(
        '/'         => 'Home:index'
        '/posts'    => array('get'  => 'Posts:index',
                         'post' => array('Posts:create', function() { echo 'Middleware!'; })),
        '/posts:id' => 'Posts:show'
    ));


**ORM :**  http://idiorm.readthedocs.org/en/latest/

##Create project composer

`composer create-project apipemc/slim-skeleton --stability=dev project-name`
